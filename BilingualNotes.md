# Bilingual String Notes

## Introduction
In CKAN documentation, the multilingual solution using ITranslate interface involves in 2 parts: 
   - extract the target strings into a plain text message file
   - then compile into a message object file for CKAN application to load.
In our implementation, the target string for translation is manually created in the plain text message file with ".pot" extension. 
Then ".po" and ".mo" files are created and compiled using either GNU or Python utilities.
Both English and French version are maintained because the target string to be replace may be longer than short text entry.
Maintaining longer entries in the translation files allow esier mainatenance and quicker search and replace by the system.

The translation module thus includes 3 files for each language under the directory path `lli-ckan/ckanext/aafc/i18n/`  
.pot - template located under `18n`
.po - readable translated text located under `i18n/en/LV_MESSAGES` and `i18n/en/LV_MESSAGES`
.mo - compiled version of tranlsated text located under `i18n/en/LV_MESSAGES` and `i18n/en/LV_MESSAGES`

## The overall process 

The processing of translated files can be done using either GNU or Python utilities.
The commands are executed on the path `app/ckan/venv/src/lli-ckan`

The target string is inseted into the code in the format of {{ _("target_string") }}
In translation template `.pot` file, insert the target string as a new key-value pair in the following format
```
msgid "target_string"
msgstr ""
``` 
Execute `msgmerge` or `update_catalog` command to insert the new key-value pair entry into the `.po` files
In the `.po` files, replace the "msgstr" value of the new key-value pair with translated or alternate text (default is msgid is msgstr is "")
Execute `msgfmt` or `compile_catalog` command to compile the `.po` file into an `.mo` file
The content in the `.mo` file will replace the target "msgid" in the code at runtime

The compiled messages should load into refreshed web pages or after a restart of the application is implemented.

## The GNU and Python utilities with commands to be used (and can be run in a shell script)   

### Using GNU utility to update exiting translation files (executed on the path `app/ckan/venv/src/lli-ckan`)
Info on GNU gettext utilities documents for translation https://www.gnu.org/software/gettext/manual/html_node/index.html#SEC_Contents 

#### GNU command to update existing PO files
msgmerge -U --no-fuzzy-matching --lang=fr --backup=simple --suffix=.backup ckanext/aafc/i18n/fr/LC_MESSAGES/ckanext-aafc.po ckanext/aafc/i18n/ckanext-aafc.pot
msgmerge -U --no-fuzzy-matching --lang=en --backup=simple --suffix=.backup ckanext/aafc/i18n/en/LC_MESSAGES/ckanext-aafc.po ckanext/aafc/i18n/ckanext-aafc.pot
#### GNU command to compile of PO files into binary MO files
msgfmt ckanext/aafc/i18n/fr/LC_MESSAGES/ckanext-aafc.po -o ckanext/aafc/i18n/fr/LC_MESSAGES/ckanext-aafc.mo
msgfmt ckanext/aafc/i18n/en/LC_MESSAGES/ckanext-aafc.po -o ckanext/aafc/i18n/en/LC_MESSAGES/ckanext-aafc.mo

#### Note: Backup file option 
Will be only be produced when there are changes
The "suffix" value may be adjusted to another value as required
Numbered backups can be implemented by changing the "backup" value from "simple" to "numbered"

### Using Python utility to update exiting translation files (executed on the path `app/ckan/venv/src/lli-ckan`)
CKAN python utilities documents for translation https://docs.ckan.org/en/latest/contributing/i18n.html 

. /app/ckan/venv/bin/activate
#### Python command to update existing PO files
python setup.py update_catalog -N --locale fr -i ckanext/aafc/i18n/ckanext-aafc.pot -o ckanext/aafc/i18n/fr/LC_MESSAGES/ckanext-aafc.po
python setup.py update_catalog -N --locale en -i ckanext/aafc/i18n/ckanext-aafc.pot -o ckanext/aafc/i18n/en/LC_MESSAGES/ckanext-aafc.po
#### Python command to compile PO files into binary MO files
python setup.py compile_catalog --locale fr -i ckanext/aafc/i18n/fr/LC_MESSAGES/ckanext-aafc.po -o ckanext/aafc/i18n/fr/LC_MESSAGES/ckanext-aafc_t1.mo
python setup.py compile_catalog --locale en -i ckanext/aafc/i18n/en/LC_MESSAGES/ckanext-aafc.po -o ckanext/aafc/i18n/en/LC_MESSAGES/ckanext-aafc_t1.mo
deactivate


