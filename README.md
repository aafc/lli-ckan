
# ckanext-aafc

- Tested and working with CKAN v2.9.

- Initial start carrying out proof of concept stage and dev.

Goal to reuse and align with Government of Canada CKAN with added customization or stripping uneeded complexity for local Agriculture and Agri-Foods (Government of Canada federal department) implementation. 

Working area right now. Some items are present for central location and change tracking only.

Licensed under Government of Canada Open Source and Open Source MIT agreements.
(Detailed in LICENSE file)

# Setup

Note: The solr.xml must be loaded instead of the out-of-the-box CKAN schema. This is to index French translations.


------------
Requirements
------------

This extension relies on `ckanext-fluet` and a custom branch of `ckanext-scheming`.


------------
Installation
------------

To install ckanext-aafc:

1. Activate your CKAN virtual environment, for example:

    `. /app/ckan/venv/bin/activate`

2. Install the ckanext-aafc Python package into your virtual environment:

     `pip install ckanext-aafc`

3. Add ``aafc`` to the ``ckan.plugins`` setting in your CKAN config file:
   
   ``/app/ckan/config/dev.ini``

4. Restart CKAN:

     `ckan --config /app/ckan/config/dev.ini run`


---------------
Config Settings
---------------

Add the AAFC extension (and its dependencies) to your CKAN config file. Remember that plugins are loaded in order. If you override core ckan in 2 plugins, the last plugin loaded will win..

    ckan.plugins = scheming_datasets scheming_groups scheming_organizations aafc fluent 
    ckanext.aafc.some_setting = some_default_value

    scheming.dataset_fallback = false
    scheming.presets=ckanext.scheming:presets.json ckanext.fluent:presets.json ckanext.aafc:schemas/tbs_presets.yaml ckanext.aafc:schemas/aafc_presets.yaml
    scheming.organization_schemas=ckanext.aafc:schemas/aafc_org.yaml
    scheming.dataset_schemas=ckanext.aafc:schemas/aafc_wg.yaml
	
To make email notification correctly, in addtion uncomment and put correct host/port/credential values in the smpt setting, following AAFC specifc variables need to be added:
    aafc_mail_from = <from address>
    aafc_mail_to = <to address>
    aafc_subject = <default subject showing in update email notification>
    aafc_subject_created = <subject for create notification>
    aafc_storage = <url of nextcloud storage>
	

Anytime translation files are updated run this in the plugin on the ec2 instance:

    python setup.py compile_catalog --locale fr

Anytime translation files are updated run this in the plugin on the ec2 instance:

    python setup.py compile_catalog --locale fr



------------------------
Development Installation
------------------------

To install ckanext-aafc for development, activate your CKAN virtualenv and
do::

    git clone https://github.com/aafc-ckan/ckanext-aafc.git
    cd ckanext-aafc
    python setup.py develop
    pip install -r dev-requirements.txt



# History
* Release1.1a -- 2021-04-21
