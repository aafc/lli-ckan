import json
import os
import re
import logging
#import urllib2
from urllib.request import urlopen
import urllib.request
from datetime import datetime, timedelta
import yaml
from ckanapi import RemoteCKAN
import requests

logging.basicConfig(filename='harvester.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO)

def query_with_get( site, apicall, q_param='', apikey = None):
    """
    Query remote site with get
    :param site:
    :param apicall:
    :param q_param:
    :param apikey:
    :return:
    """

    url1 = site + apicall
    req = urllib.request.Request(url1 + q_param)
    if apikey != None:
        req.add_header('Authorization', apikey)
    # response = urllib.request.urlopen(req)
    # For Python 2.7
    response = None
    try:
        response = urlopen(req)
    except urllib.error.HTTPError as e:
        # logging.info("might not have the package in OG site yet")
        return None
    except:
        logging.info("Error happening querying Registry site")
        return None
    res = response.read()

    return res


def purge_dataset(package_ids, source = False):
    '''

    :param package_ids: list of package id
    :return:
    '''
    site = os.getenv("destination_url")
    api_key = os.getenv("destination_api_key")

    if source:
        site = os.getenv("source_url")
        api_key = os.getenv("source_api_key")
    session = requests.Session()
    session.verify = False

    rckan = RemoteCKAN(site, apikey=api_key,session=session)

    count = 0
    ret = []
    try:
        for package_id in package_ids:
            data_as_d = {"id": package_id}
            ret = rckan.call_action("dataset_purge", data_dict=data_as_d)
            logging.info("count:%d"%count+ json.dumps(ret))
            count += 1
    except Exception as e:
        # if no data exists yet, return empty
        pass
    return ret


def load_json(file):
    data =  None
    with open(file) as json_fp:
        data = json.load(json_fp)
    return data


region_mappings = { 10:(1001,1011),11:(1101,1103),12:(1201,1218),13:(1301,1315),2:(2401,2499),3:(3501,3560),
                    46:(4601,4623),47:(4701,4718),48:(4801,4819),5:(5901,5959),60:(6000,6002),61:(6101,6106),62:(6204,6208)}


def map_regions(reg_large):
    '''
    Another way to map large number to small number
    :param reg_large:
    :return:
    '''
    for reg_small, min_max in region_mappings.items():
        if reg_large> min_max[0] and reg_large < min_max[1]:
            return str(reg_small)
    return None


def replace_regions(og_data):
    '''
    Called by:
    * create_to_registry()
    * update_to_registry()
    Call:
    * map_regions()
    '''
    if 'place_of_publication' in og_data and og_data['place_of_publication'] != []:
        pub_int = int(''.join(og_data['place_of_publication']))
        pub_reg = map_regions(pub_int)
        og_data['place_of_publication'] = [pub_reg]
    if 'geographic_region' in og_data and og_data['geographic_region'] != []:
        geo_int = int(''.join(og_data['geographic_region']))
        geo_reg = map_regions(geo_int)
        og_data['geographic_region'] = [geo_reg]
    return og_data


def get_all_data_og():
    '''

    :return:
    '''
    source_url = os.getenv("og_source_url")
    source_api_key = None
    rows = os.getenv("rows_to_get_og")
    session = requests.Session()
    session.verify = False
    rckan = RemoteCKAN(source_url, apikey=source_api_key, get_only=True,session=session)
    param = {"rows":rows, "q":"organization:aafc-aac"}
    result = rckan.call_action("package_search",param)
    return result


def get_all_data(isSink = False, timelimit = False, restrict=False):
    '''
    retrieve all data from the remote site
    :return:
    '''
    #Get the lastest list from registry
    data_url = os.getenv("source_url")
    data_api_key = os.getenv("source_api_key")
    if isSink:
        data_url = os.getenv("destination_url")
        data_api_key = os.getenv("destination_api_key")
    rows = os.getenv("rows_to_get")

    if data_url == None or len(data_url) == 0 or data_url.find("http") == -1:
        # logging.info("Missing url")
        return
    if data_api_key == None or len(data_api_key) == 0:
        logging.info("Missing api key")
        return
    
    session = requests.Session()
    session.verify = False
    rckan = RemoteCKAN(data_url, apikey=data_api_key,session=session)
    param = {"rows": 1000}
    # if timelimit:
    #     param["q"] = calculate_time_param()

    # Return 1000 results which is CKAN Max. Default/no param is 10
    result = rckan.call_action("package_search", param)
    # logging.info("Retrieved data from remote site")
    if restrict:
        new_result={}
        new_result["count"] = result["count"]
        new_result["results"] = []
        for i in result['results']:
            kwes=[]
            kws = i["keywords"]["en"]
            # logging.info(kws)
            remove = True
            for kw in kws:
                # logging.info(kw)
                kwes.append(kw)
                match = re.match("(Living.?Labs?)", kw, re.IGNORECASE)
                if match:
                    remove = False
            # str_kw = ",".join(kwes)
            #logging.info(">>>List of kw in e:%s"%str_kw)
            if not remove:
            #    result['results'].remove(i)
            #else:
                new_result['results'].append(i)
        result = new_result
    return result


def calculate_time_param():
    return
    hours_ago = int(os.getenv("time_limit"))
    if hours_ago == None:
        hours_ago = 48 #default
    hours_ago = 1000
    two_days_ago = datetime.now() - timedelta(hours=hours_ago)
    str_ndays_ago = two_days_ago.strftime('%Y-%m-%dT%H:%M:%SZ')
    #q_param0 = "?q=metadata_modified:[%s%sTO%s*]" % (str_ndays_ago, '%20', '%20')
    q_param = "metadata_modified:[%s TO *]" % (str_ndays_ago)
    return q_param

## one time used utilities
def generate_kw_reverse_dict():
    """
    use a canada_keywords segment of schema to generate a reverse dictionary which
    the key is english label and value is the value of a choice
    :return:
    """
    with open("Data/keywords.yaml","r") as fo:
        kw = yaml.load(fo)

    reverse_dict = {}
    for i in kw[0]["values"]["choices"]:
        label_e = i["label"]["en"]
        reverse_dict[label_e] = i["value"]

    with open("Data/reverse_kw_dict.json", "w") as fo:
        json.dump(reverse_dict, fo)

def get_all_ids(remote=True):
    '''
    Get all ids from site
    :param remote:
    :return:
    '''
    if remote:
        site = os.getenv("source_url")
        api_key = os.getenv("source_api_key")
    else: #local
        site = os.getenv("destination_url")
        api_key = os.getenv("destination_api_key")
    session = requests.Session()
    session.verify = False
    rckan = RemoteCKAN(site, apikey=api_key,session=session)
    try:
        ret = rckan.call_action("package_list")
        #for id in ret:
        #    logging.info(id)
    except Exception as e:
        logging.info("Error message : %s" % e.message)
        ret = []
    return ret