#!/usr/bin/env python
import logging
import os
import re
import json
from datetime import datetime, timedelta
import urllib
import requests
from random import randint
from dotenv import load_dotenv

from ckanapi import RemoteCKAN
from ckanapi.errors import *

# hmmm
from helper import get_all_data

logging.basicConfig(filename='harvester.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO)

# This is unique to single env; Need to change
to_replace = {
    "owner_org": "5bb66c6c-5549-41a1-82fc-d9b5c04670f8" ,
    "org_title_at_publication":"agriculture_and_agrifood_canada" 
    }

def load_json(file):
    data =  None
    with open(file) as json_fp:
        data = json.load(json_fp)
    return data

def location_lookup(raw_location):
    location = ''
    loc = raw_location.lower()
    # logging.info('raw location')
    # logging.info(loc)
    if 'quebec' in loc:
        location = "livinglabquebec"
    elif 'eastern p' in loc:
        location = "livinglabeasternp"
    elif 'atlantic' in loc:
        location = "livinglabatl"
    elif 'ontar' in loc:
        location = "livinglabontario"
    elif 'british co' in loc:
        location = "livinglabbritishcolumbia"
    return location

def nightmare_hack(raw_location):
    location_key = ''
    match = re.match("(Living.?Labs?)", raw_location, re.IGNORECASE)
    # if living labs in kew
    if match:       
        location_key = location_lookup(raw_location)
    return location_key

def process_keywords(data, check_against):
    '''
    make keywords compatible to lli
    :param data:
    :param check_against:
    :return:
    '''
    check_against0 = check_against[0]
    check_against1 = check_against[1]
    check_against2 = check_against[2]
    kw_replacing = []
    location = []
    theme =[]
    
    if "keywords" in data.keys():
        kw_english = data["keywords"]["en"]
        for k in kw_english:
            k2 = k[0].upper() + k[1:]
            if k2 in check_against0:
                kw_replacing.append(check_against0[k2])
        for k in kw_english: # Location
            # logging.info(k)
            true_location = nightmare_hack(k)
            if true_location:
                # logging.info("True location: {}".format(true_location))
                location.append(true_location)
        for k in kw_english: # Theme
            k2 = k[0].upper() + k[1:]
            if k2 in check_against2:
                theme.append(check_against2[k2])
    
    if (len(kw_replacing) == 0):
        logging.info("Missing keywords! Setting keyword to 'unknown' for '{}'".format(data['title_translated']['en']))
        logging.info('Keywords: {}'.format(kw_english))
        kw_replacing.append("unknown")
    if (len(theme) == 0):
        logging.info("Missing theme! Setting theme to 'general' for '{}'".format(data['title_translated']['en']))
        logging.info('Keywords: {}'.format(kw_english))
        theme.append(u'general')
    
    if (len(location) == 0):   
        logging.error("missing location! Not Harvesting: {}".format(data['title_translated']['en']))
        logging.error('Keywords: {}'.format(kw_english))
        return None

    data["keywords"] = kw_replacing
    data[u'theme'] = theme
    data[u'location'] = location
    return data


def temp_process(data):
    if "frequency" in data.keys():
        if data["frequency"] in ("not_planned","unknown"):
            data["frequency"] = "as_needed"

    if len(data['resources']) > 0:
        for r in data['resources']:
            if "language" in r.keys():
                for l in r["language"]:
                    if l == "bilingual":
                        idx = r["language"].index("bilingual")
                        r["language"][idx] = "other"
            else:
                logging.info("no language in resource")


def transform_data(data, keys_to_remove,items_to_add, kw_dict, items_to_replace = None):
    '''
    Transform the incoming data by remove some items and add some items with default value to match the
    target schema

    :param data:
    :param keys_to_remove:
    :param items_to_add:
    :return:
    '''
    # Set org
    owner_org_id = os.getenv("organization_id", None)
    if owner_org_id != None:
        items_to_replace['owner_org'] = owner_org_id

    transformed = []
    for d in data:
        # remove items
        for key in keys_to_remove:
            d.pop(key,"default")
        for k,v in items_to_add.items():
            d[k] = v
        for k, v in items_to_replace.items():
            d[k] = v
        # remove "extra" fields for some old Registry datasets
        d.pop("extras","default")

        # if d["id"] in ["dafded61-61a2-478d-91f6-2c77f030214b"]:
        #     pass
        #d["keywords"]
        result = process_keywords(d, kw_dict)

        #other temperary conversion to get around
        if result:
            temp_process(d)
            transformed.append(d)
    # logging.info('TRANSFORMED ==============')
    # logging.info(transformed)
    return transformed

def extract_n_transform(from_og=False):
    restrict_env = os.getenv("restricted")
    lli_only = True
    timelimit = True if os.getenv("time_limit", None) != None else False
    all_data = get_all_data(restrict=lli_only, timelimit=timelimit)

    keys_to_remove = load_json("Data/keysToRemove.json")
    items_to_add = load_json("Data/fieldsAddedForLli.json")
    rev_dict = load_json("Data/reverse_kw_dict.json")

    extracted = all_data["results"]

    date_str = datetime.now().strftime("%Y%m%d_%H%M%S")
    # with open(f"Data/{date_str}_extracted.json","w") as fo:
    #     json.dump(extracted, fo, indent=4)

    return transform_data(
        extracted, keys_to_remove, 
        items_to_add, rev_dict, to_replace
    )

def log_info( file, id, message=""):
    with open(file , "a") as fout:
        now = datetime.now()
        event = "%s with id %s on %s\n" % (message,id, now)
        fout.write(event)

def harvest_registry(from_og=False, id=None, extra_params={}):
    '''
    Harvest data from Registry and transform to LLI format
    :param from_og: if True, harvest from OG, otherwise harvest from Registry
    :param id: if not None, harvest only the dataset with the given id
    :param extra_params: a dictionary of extra parameters
    '''
    dest = os.getenv("destination_url")
    source = os.getenv("source_url")
    dest_key = os.getenv("destination_api_key")

    if dest == None or len(dest) == 0 or dest.find("http") == -1:
        logging.info("variable 'site' is missing")
        return
    if dest_key == None or len(dest_key) == 0:
        logging.info("destination api key is missing")
        return

    session = requests.Session()
    #FIXME Hack around bad cert
    session.verify = False
    rckan = RemoteCKAN(dest, apikey=dest_key,session=session)

    logging.info("Harvesting data from {} to destination {}".format(source, dest))

    # default from remote site
    if "transformed_data" not in extra_params:
        data = extract_n_transform(from_og)
        logging.info('transforming!')
    else:
        pass

    # save transformed data to file if indicated
    if extra_params.get("save_to_file", False):
        date_str = datetime.now().strftime("%Y%m%d_%H%M%S")
        # with open(f"output/{date_str}_transformed.json","w") as fo:
        #     json.dump(data, fo, indent=4)
            # fo.write(json.dumps(data))
    #post to destination
    confirmed_ids = []
    dataset_name = []
    for d in data:
        # with open(f"output/upload_dataset.json","w") as fo:
        #     json.dump(d, fo, indent=4)
        # logging.info(d)
        try:
            ret = rckan.call_action("package_create", data_dict=d)
        except ValidationError as ev:
            logging.error("Failure pulling, validation error")
            # logging.info(d['title_translated']['en'])
            # logging.info(v)
            ignore = False
            for k, v in ev.error_dict.items():
                # ignore __type
                if k == '__type':
                    continue
                logging.info(" %s : %s for %s ID: %s" % (k, v, d['title_translated']['en'], d['id'] ))
                if len(v) > 0 and v[0] == u'Dataset id already exists':
                    ignore = True
            if ignore: # duplicate id
                continue
            log_info("error_sync_with_registry.log", d['id'], "Failure pulling, validation error")
            continue
        except CKANAPIError as ec:
            logging.info("Failure creating package, CKANAPI error")
            # logging.info(d)
            # logging.info(ec)
            # log_info(ec.extra_msg)
        except Exception as e:
            logging.info("Failure posting, other reason ")
            for k, v in e.error_dict.items():
                if k == '__type':
                    continue
                logging.info(" %s : %s" % (k, v))
            log_info("error_sync_with_registry.log", d['id'], "Failure pulling, other reason")
            continue
        confirmed_ids.append(d['id'])
        dataset_name.append(d['title_translated']['en'])

    with open("harvester.success.log", "a") as fout:
        now = datetime.now()
        names = "\n".join(dataset_name)    
        event = "Harvester ran at: %s\n %s\n" % (now, names)
        fout.write(event)

def main():
    # Write to debug files
    save_to_file = os.getenv("save_harvested_datasets",False)
    transformed = os.getenv("transformed_data",None)
    params = {"save_to_file":save_to_file}
    if transformed != None:
        params["transformed_data"] = transformed

    harvest_registry(extra_params=params)

if __name__ == "__main__":
    load_dotenv()
    main()