# Note about harvest script


## Required
You will need to populate the .env environment with API keys from both harvesting
source_url=
source_api_key

destination_url=
destination_api_key=

## Brief:
Data scripts in this foler were from registery project with some of the code rewritten based on new requirements and better knowledge about the CKAN application. Some early scripts for CKAN are still kept but not used

Scripts:
* data_cleansing -- Used by Registry. not used in LLI
* update_empty_records -- Used by Registry. not used in LLI
* send_email -- Used by Registry. Can be reused in LLI if needed
* helper -- Supporting functions used by other scripts
* data_utility -- Some tool functions, including
  * purge all data in a LLI CKAN app
  * dump harvested data to a file
  * load saved data to LLI CKAN app
* synch_with_og -- The main harvest script. Rewritten in a way that harvests data from source to destination based on configuration
* publish_to_og -- The main push script. Push the latest data from LLI (still called destination) to Registry(Still called source)

## Usage and notes:
In real production, a initial loading run of the script should be run to have most data ready. Then a daily scheduled job should be running to capture any new data in source.

Currenlty only the initial loading has been tested in LLI QA environement with no filtering condition because if filtered the would be no data for harvest.

Job scheduling should used cron but any solution can be used.According to Registry's practise, the daily scheduled job has very little data traffic for now.

All the configuration is in .env file. The configuration parameters are self-explained.

## Test Procedure:
The main scripts to test are:
* synch_with_og -- pull data from Registry
* publish_to_og -- push new LLI data to Registry
Both scripts can be run under the same machine that hosting CKAN or a local machine of developer/tester. Following are the steps to test

**Step 1:** Install required modules
On a machine that have Python 3.x installed, run:

`pip install -r requirements.txt`


**Step 2:** Make the environment variable ready
Use any editor to modify the file ".env", fill in following variable:
source_url -- This is registry ckan website
source_api_key -- This is an api key of an acount on Registry CKAN Website 
destination_url -- This is LLI test ckan web site 
destination_api_key -- This is an api key of an acount on LLI CKAN Website
(optional) rows_to_get -- The number of maximum data retireve from Registry


**Step 3:** Purge all the  LLI data set. Run

`python data_utility -f purge`

The living lab CKAN website should be empty now.


**Step 4:** Test harvest. Run

`python synch_with_og`

The Living Lab CKAN should have new datasets from Registy.


**Step 5:** Test pushing. Create a new dataset on livinglab and make it open. Run

`python publish_to_og`

The new data should be on Registry








