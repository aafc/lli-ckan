#!/bin/bash
# Script to run scheduled task scripts

source /app/ckan/venv/bin/activate            
cd /app/ckan/venv/src/livinglabs/contrib/etl/    
python synch_with_og.py
python publish_to_og.py