# extracted from 'pull_from_reg'

def get_filtered_data():
    '''
    Instead of harvesting all data, filter those data with special keywords
    :return:
    '''
    source_url = os.getenv("source_url")
    source_api_key = os.getenv("source_api_key")
    rows = os.getenv("rows_to_get")
    session = requests.Session()
    session.verify = False
    rckan = RemoteCKAN(source_url, apikey=source_api_key,session=session)
    search_for = os.getenv("search_for")
    param = {"rows":30,"q":search_for}
    param = {"rows": 30}
    api_res = rckan.call_action("package_search",param)
    results = api_res['results']
    filtered = []
    for d in results:
        if search_for in d['keywords']['en'] or search_for in d['keywords']['fr']:
            filtered.append(d)

    return filtered

def test_process_keywords():
    rev_dict = load_json("Data/reverse_kw_dict.json")
    raw_data = load_json("Data/extracted.json")
    for d in raw_data:
        print(json.dumps(d["keywords"]))
        d2 = process_keywors(d, rev_dict)
        print(json.dumps(d["keywords"]))

def test_search_filter():
    filtered_res = get_filtered_data()
    
def test_keys(file1, file2):
    '''
    pick up any keys in dictionary1 not exist in dictionary2
    :param file1:
    :param file2:
    :return:
    '''
    data1 = load_json(file1)
    data2 = load_json(file2)
    if data1 != None and data2 != None:
        for k,v in data1.items():
            if k in data2:
                continue
            print(k)

# extracted from publish_to_og
def test_push_convert():
    data = load_json("Data/forPush2.json")[0]

    for k in to_convert:
        if data.has_key(k):
            data[k] = conversion[k](data[k])
        else:
            data[k] = conversion[k]()
    with open("Data/testConvert.json",'w') as fout:
        json.dump(data,fout)