#!/usr/bin/env python
# -*- coding: cp1250 -*-
from dotenv import load_dotenv
from datetime import datetime, timedelta
from pprint import pprint
from helper import *
#from helper import purge_remote_data
from ckanapi import errors

'''
THE NAME OF THE SCRIPT IS MISLEADING BECAUSE:
The script was developed for Registry project to push data to OG. Now  with some  modification it's also used 
in LLI project to push data to Registry. The names of functions and variable names were adjust to make it
more general.

'''

# following fields were left over of old version. They are for registry only
to_remove = ["aafc_sector", "procured_data", "procured_data_organization_name", "authoritative_source",
             "drf_program_inventory", "data_steward_email", "elegible_for_release", "publication",
             "data_source_repository", "aafc_note", "drf_core_responsibilities",
             "mint_a_doi", "other", "ineligibility_reason", "authority_to_release", "privacy", "formats", "security",
             "official_language", "access_to_information", "organization", "groups"
             ]
to_replace = {"type": "dataset", "owner_org": "45826094-5c88-4ae8-803f-a684012cfdb2",
              "collection": "primary", "jurisdiction": "federal",
              "procured_data_organization_name":"N/A"}

to_convert = ["keywords", "org_title_at_publication", "odi_reference_number","resources"]

to_enforce_mandatory = {"creator":"creator@agc.gc.ca", "resources":{"date_published":"2000-01-01"}}

def convert_kw(kw):
    kw_lli = {}
    with open("Data/keywords_as.json", "r") as fi: #reverse the keywords from lli format to normal format
        kw_lli = json.load(fi)
    converted = {"fr": [], "en": []}
    for i in kw:
        if i in kw_lli: #put en and fr instead of key for text
            converted["en"].append(kw_lli[i][0])
            converted["fr"].append(kw_lli[i][1])
        else:
            converted["en"].append(i)
            converted["fr"].append(i)
    return converted


def convert_orgt(orgt):
    converted = {}
    converted["en"] = orgt
    converted["fr"] = orgt
    return converted


def convert_odi(original = None):
    return "ODI-2018-2222"

def convert_resource(resources):
    for res in resources:
        for k, v in res.items():
            if k == 'language':
                new_v = []
                for lang in v:
                    if lang == 'bilingual':
                        new_v.append('other')
                    else:
                        new_v.append(lang)
                res[k] = new_v
            if k == 'date_published' and v == '':
                res[k] = str(datetime.today())

    return resources

conversion = {"keywords": convert_kw, "org_title_at_publication": convert_orgt,
                  "odi_reference_number": convert_odi,"resources":convert_resource}


def get_data_from_local(package_id):  # here local means lli or registry based on configuration
    """
    Get data from local CKAN
    :param package_id:
    :return:
    """
    site = os.getenv("destination_url")  # sink or local. For LLI it's lli, For Registry it's Regisgtry
    rckan = RemoteCKAN(site)

    data_as_d = {"id": package_id}
    try:
        ret = rckan.call_action("package_show", data_dict=data_as_d)  # data_as_dict )
    except Exception as e:
        ret = ""
        print("failed")

    return ret

def get_n_dump(package_id):
    '''
    Get data from 'local' CKAN app and dump to json file. 'local' means lli or registry based on configuration, not local docer app.
    '''
    og_data = get_data_from_local(package_id)
    with open("Data/"+package_id+"_fromLliLocal.json","w") as fo:
        json.dump(og_data, fo, indent=4)
    return og_data


def get_n_post(package_id, test=False, lli=True):
    """
    Get an data from registry , modify and post it to OG
    :param lli:
    :param test:
    :param package_id:
    :return:
    """
    og_data = get_n_dump(package_id)
    return push_a_package(package_id, test, lli)      


def push_a_package( package_id, test=False,lli=True ):
    with open ("Data/"+package_id+"_fromLliLocal.json", "r") as fi:
        og_data = json.load(fi)
    # replace
    for k, v in to_replace.items():
        if lli and k == "type":
            continue
        og_data[k] = to_replace[k]

    if lli:  # Only convert under lli
        for k in to_convert:
            if k in og_data.keys():
                og_data[k] = conversion[k](og_data[k])
            else:
                og_data[k] = conversion[k]()
    to_remove_lli = load_json("Data/fieldsAddedForLli.json").keys()
    if lli:  # When the case is lli as oppose to registry
        to_remove_final = to_remove_lli
    else:
        to_remove_final = to_remove  # keep original version which was developed for Registry
    # remove
    for k in to_remove_final:
        del og_data[k]
    # check mandatory, if missing value then add the default value
    # Only in lli
    for k, v in to_enforce_mandatory.items():
        if k in og_data.keys():
            if isinstance(v, dict):
                for kk, vv in v.items():
                    for res  in og_data[k]:
                        if kk in res.keys():
                            if res[kk] == "":
                                og_data[k][kk] = vv 
                        else:
                            res[kk] = vv

            else:
                if og_data[k] == "":
                    og_data[k] = v
        else:
            og_data[k] = v

    remote_site = os.getenv("source_url")
    remote_key = os.getenv("source_api_key")
    session = requests.Session()
    session.verify = False
    rckan = RemoteCKAN(remote_site, apikey=remote_key,session=session)

    if test:
        file_name = "%s_before_push.json" % package_id
        with open("Data/" + file_name, "w") as fo:
            json.dump(og_data, fo, indent=4)
        return False
    # First try to create new package.
    # If package create fails, it's possible that the package already exists
    # Try to update the package. If both of these actions fail, return false.
    og_data['__type'] = 'internal'
    og_data['owner_org'] = 'aafc-7c-aac'
    try:
        ret = rckan.call_action("package_create", data_dict=og_data)

    except errors.ValidationError as ve:
        for k,v in ve.error_dict.items():
            print("validation error key: %s"%k)
            print("validation error val: %s" % v)
    except  errors.SearchIndexError as se:
        print("search index error: %s" % se.extra_msg)
    except Exception as e1:
        print("error e1:%s"%e1.message)
        try:
            ret = rckan.call_action("package_update", data_dict=og_data)
        except Exception as e2:
            print("error e2:%s" % e2.message)
            return False
        return True

    return True


def get_ids():
    site = os.getenv("livinglabs_url")
    rckan = RemoteCKAN(site)

    # query for last 48 hours
    apicall = "api/3/action/package_search"
    # q_param = "?q=metadata_modified:[2019-10-10T21:15:00Z TO *]&fq=publication:open_government"

    hours_ago = 48
    two_days_ago = datetime.now() - timedelta(hours=hours_ago)
    str_2days_ago = two_days_ago.strftime('%Y-%m-%dT%H:%M:%SZ')
    # q_param1 = "?q=metadata_modified:[%s%sTO%s*]" % (str_2days_ago, '%20', '%20')
    q_param1 = ''
    res = query_with_get(site, apicall, q_param1)
    dict = json.loads(res)['result']['results']

    # additionally filter only records where open checklist criteria passes
    filtered_dict = [x for x in dict if (
            x['ready_to_publish'] == 'true'
            and x['elegible_for_release'] == 'true'
            and x['access_to_information'] == 'true'
            and x['authority_to_release'] == 'true'
            and x['formats'] == 'true'
            and x['privacy'] == 'true'
            and x['official_language'] == 'true'
            and x['security'] == 'true'
            and x['other'] == 'true'
            and x['imso_approval'] == 'true'
            and x['license_id'] == 'ca-ogl-lgo'
            and x['restrictions'] == 'unrestricted')]

    print (filtered_dict)
    # process the result to get filtered ids

    try:
        ids = []
        for index in range(len(filtered_dict)):
            ids.append(filtered_dict[index]['name'])
    except Exception as e:
        return []
    print(ids)
    return ids





def pull_data(local=True):
    """
    pull data and save to json file. Using a flag to decide remote(source) or local(destination)
    :param local: default True as pull data from harvest destination
    :return:
    """

    # id = "3044d83b-edac-4b38-87fd-1b54730dbec4"
    package_id = "3055d83b-edac-4b38-87fd-1b54730dbec4"
    if local:
        site = os.getenv("destination_url")
        key = os.getenv("destination_api_key")
    else:
        site = os.getenv("source_url")
        key = os.getenv("source_api_key")
    session = requests.Session()
    session.verify = False
    rckan = RemoteCKAN(site, apikey=key,session=session)
    data_as_d = {"id": package_id}
    data = rckan.call_action("package_show", data_dict=data_as_d)

    filename = "%s_%s.json" % (package_id, local)
    with open("Data/" + filename, "w") as fo:
        json.dump(data, fo, indent=4)

def query_local_for_hours_ago(hours_ago = 48 ):
    site = os.getenv("destination_url")
    key = os.getenv("destination_api_key")
    session = requests.Session()
    session.verify = False
    rckan = RemoteCKAN(site, apikey=key,session=session)

    # query for last 48 hours
    apicall = "api/3/action/package_search"
    # q_param = "?q=metadata_modified:[2019-10-10T21:15:00Z TO *]&fq=publication:open_government"

    two_days_ago = datetime.now() - timedelta(hours=hours_ago)
    str_2days_ago = two_days_ago.strftime('%Y-%m-%dT%H:%M:%SZ')
    #q_param1 = "?q=metadata_modified:[%s%sTO%s*]" % (str_2days_ago, '%20', '%20')
    # q_param1 = "?q=metadata_created:[%s%sTO%s*]" % (str_2days_ago, '%20', '%20')
    # q_param = "?q=metadata_modified:[%s%sTO%s*]" % (str_2days_ago, '%20', '%20')
    res = query_with_get(site, apicall)
    dict = json.loads(res)['result']['results']

    return dict

def test_query():
    res =query_local_for_hours_ago(48)
    print(len(res))
    return res


def do_push():
    """
    Push a single dataset from LLI to Registry
    :return:
    """
    # package_id = "46918f96-7710-45b6-939d-8a198eac5859"
    package_id = "3055d83b-edac-4b38-87fd-1b54730dbec4"
    query_list = query_local_for_hours_ago()
    if len(query_list) == 0:
        with open("publisher.success.log", "a") as fout:
            now = datetime.now()
            event = "No new dataset need to push on %s\n" % ( now)
            fout.write(event)
        return
    #package_id = query_list[0]["id"]
    #get_n_post(package_id)

    remote_ids = get_all_ids()
    for package in query_list:
        id = package['id']
        if id in remote_ids:
            continue #skip ids already done
        #res = get_n_post(id, test=True) # for dumping middle result
        res = get_n_post(id)
        if res is False:
            with open("error_post_to_og.log", "a") as fout:
                now = datetime.now()
                event = "Failed publishing package id %s on %s\n" % (id, now)
                fout.write(event)
        else:
            with open("publisher.success.log", "a") as fout:
                now = datetime.now()
                event = "Succeeded publishing package id %s on %s\n" % (id, now)
                fout.write(event)



def main():
    do_push()  # grab a local data to push to remote
    # test_purge_remote() #remove remote package
    # pull_data(False) #pull raw data and save
    #test_query()
    #test_push_convert()

def main0():  # Old version main
    id_list = get_ids()
    for id in id_list:
        res = get_n_post(id)
        if res is False:
            with open("error_post_to_og.log", "a") as fout:
                now = datetime.now()
                event = "Failed publishing package id %s on %s\n" % (id, now)
                fout.write(event)
        else:
            with open("publisher.success.log", "a") as fout:
                now = datetime.now()
                event = "Succeeded publishing package id %s on %s\n" % (id, now)
                fout.write(event)


if __name__ == "__main__":
    load_dotenv()
    main()
