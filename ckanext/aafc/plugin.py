# encoding: utf-8

import logging
import os
import re
import json
import smtplib
import socket
from time import gmtime, strftime
from email.mime.text import MIMEText
from collections import OrderedDict
import yaml

import routes.mapper

from ckan.common import config, _, json, request, c, session, ungettext
import ckan.authz
from ckan.lib.i18n import get_lang
import ckan.plugins as p
import ckan.plugins.toolkit as tk
from ckan.plugins.toolkit import chained_action, side_effect_free
from ckan.lib.plugins import DefaultDatasetForm, DefaultTranslation
from ckan.lib import app_globals as globals
import ckan.lib.helpers as h
import ckan.lib.base as base

# from ckan.logic import validators as logic_validators

# import ckanapi
from ckanext.scheming import helpers as sh

from ckanext.aafc import validators
from ckanext.aafc import helpers
# from ckanext.aafc import activity as act
# from ckanext.aafc.extendedactivity.plugins import IActivity
import ckanext.aafc.blueprint as blueprint
from ckanext.aafc.data import reverse_ctrlvoc_values

from ckan.views.dataset import search as dataset_search

log = logging.getLogger(__name__)

def addgeopage():
    return tk.render('/home/addgeo.html')

def disclaimer():
    return tk.render('/home/disclaimer')

def cdtsMenu():
    return tk.render('home/cdtsMenu-en')

@chained_action
@side_effect_free
def package_search(original_action, context, data_dict):
    return original_action(context, data_dict)

@side_effect_free
def get_keywords(context, data_dict):
    result_list = []
    with open(r'/app/ckan/venv/src/lli-ckan/ckanext/aafc/schemas/keyword_presets.yaml') as f:
        keywords = yaml.load(f, Loader=yaml.FullLoader)
        keywords = keywords['presets']
        choices = list(filter(lambda preset: preset['preset_name'] == 'canada_keywords',
            keywords))[0]['values']['choices']
        # XXX Fix this sucks
        for _dict in choices:
            if 'delete' in _dict:
                continue    
            result_list.append({
                'name': _dict['label']['fr'] if h.lang() == 'fr' else _dict['label']['en'],
                'value': _dict['value'],
                'sort': _dict['label']['ff'] if h.lang() == 'fr' else _dict['label']['en'],
                })
        new_list = sorted(result_list, key=lambda k: k['sort'].lower())
        return new_list


@side_effect_free
def get_providers(context, data_dict):
    result_list = []
    with open(r'/app/ckan/venv/src/lli-ckan/ckanext/aafc/schemas/providers_presets.yaml') as f:
        keywords = yaml.load(f, Loader=yaml.FullLoader)
        keywords = keywords['presets']
        choices = list(filter(lambda preset: preset['preset_name'] == 'aafc_org_title_at_publication',
            keywords))[0]['values']['choices']
        for _dict in choices:
            result_list.append({
                'name': _dict['label']['fr'] if h.lang() == 'fr' else _dict['label']['en'],
                'value': _dict['value'],
                'deleted': _dict.get('delete', False)
                })
        new_list = sorted(result_list, key=lambda k: k['name'].lower())
        return new_list

class AafcPlugin(p.SingletonPlugin, DefaultDatasetForm , DefaultTranslation):
    p.implements(p.IBlueprint)
    p.implements(p.IConfigurer)
    p.implements(p.ITemplateHelpers)
    p.implements(p.IActions)
    p.implements(p.IValidators, inherit=True)
    p.implements(p.IFacets)
    p.implements(p.IPackageController)
    p.implements(p.ITranslation)
    p.implements(p.IResourceController)
    # IConfigurer
    # instance variable
    extra_org_search = False

    def update_config(self, config_):
        tk.add_template_directory(config_, 'templates')
        tk.add_public_directory(config_, 'public')
        tk.add_resource('fanstatic', 'aafc')

        # XXX Fix Translation won't work here for some reason

        # config_title = ckan.plugins.toolkit._("Living Labs")
        # tk.add_ckan_admin_tab(
        #     config_, 'aafc.lli_config', config_title)

        try:
            from ckan.lib.webassets_tools import add_public_path
        except ImportError:
            pass
        else:
            asset_path = os.path.join(
                os.path.dirname(__file__), 'assets'
            )
            add_public_path(asset_path, '/')

    def get_actions(self):
        return {
            "package_search": package_search,
            "get_yaml": get_providers,
            "get_keyword_yaml": get_keywords
        }

    def i18n_locales(self):
        return ['en','fr']

    def i18n_domain(self):
        dir = self.i18n_directory() 
        return "ckanext-aafc" 

    # IValidators
    def get_validators(self):
        return {
            'canada_validate_generate_uuid':
                validators.canada_validate_generate_uuid,
            'canada_tags':
                validators.canada_tags,
            'geojson_validator':
                validators.geojson_validator,
            'email_validator':
                validators.email_validator,
#            'protect_portal_release_date':
#                validators.protect_portal_release_date,
            'canada_non_related_required':
                validators.canada_non_related_required,
            'if_empty_set_to':
                validators.if_empty_set_to,
            'selection_max_10':
                validators.scheming_max_10b,
         }

    #ITemplateHelpers
    def get_helpers(self):
        return dict((h, getattr(helpers, h)) for h in [
            'get_license',
            'lli_keywords_sort',
            'check_visibility',
            'scheming_language_text2',
            'build_org_url',
            'get_result_limit',
            'get_data_providers',
            'get_package_title',
            'get_storage_url',
            'get_keywords',
            'build_nav_icon_wcag'
            ])

    # IFacets
    def dataset_facets(self, facets_dict, package_type):
        ''' Update the facets_dict and return it. '''

        facets_dict.update({
            'organization': _(u'Organization'),
            'res_format': _(u'Format'),
            'aafc_sector': _(u'Sector'),
            'res_type': _(u'Resource Type'),
            })
        log.info(">>>facets_dict" + json.dumps(facets_dict))

        if 'groups' in facets_dict:
            del facets_dict['groups']
        if 'organization' in facets_dict:
            del facets_dict['organization']
        if 'license_id' in facets_dict:
            del facets_dict['license_id']

        #facets_dict['canada_keywords'] = tk._('Keywords')
        facets_dict['keywords'] = tk._('Keywords2')
        facets_dict['org_title_at_publication'] = tk._(
            'Organization'
        )
        lang_code = tk.request.environ['CKAN_LANG']
        if lang_code == 'en':
            facets_dict['canada_keywords'] = tk._('Keywords')
        else:
            facets_dict['canada_keywords'] = tk._('Mots clés')        
        #facets_dict['extras_location'] = p.tk._('Publisher - Organization Name at Publication')
        facets_dict['extras_theme'] = sh.scheming_get_preset("aafc_theme")['label'][lang_code]

        if lang_code == 'en':
            facets_dict['extras_location'] = 'Living Lab'
        else:
            facets_dict['extras_location'] = 'Laboratoire vivant'

        c.is_sysadmin = ckan.authz.is_sysadmin(c.user)
        
        if lang_code == 'en':
            if c.is_sysadmin: # and 'private' in facets_dict:
                facets_dict['private'] = tk._('Are there unpublished datasets?')
        else:
            if c.is_sysadmin: # and 'private' in facets_dict:
                facets_dict['private'] = tk._(u'Existe-t-il des jeux de données non publiés?')
        return facets_dict

    def organization_facets(self, facets_dict, organization_type,
                            package_type):
        return self.dataset_facets(facets_dict, package_type)

    def group_facets(self, facets_dict, group_type, package_type):
        return self.dataset_facets(facets_dict, package_type)

# IPackageController
    def read(self, entity):
        pass

    def create(self, entity):
        pass

    def edit(self, entity):
        is_created = session.get("created",False)
        log.info(f'>>>> is_created:{is_created}')
        # TODO: if is_created, change the body/subject
        if is_created:
            subject = config.get("aafc_subject_create","A dataset created/updated")
        else:
            subject = config.get("aafc_subject","A dataset created/updated")
        host = tk.request.host
        host_url = tk.request.host_url
        keys= dir(entity)
        eid = entity.id
        url = h.url_for('dataset_read', id=eid)
        state = entity.state
        site_url = config.get("ckan.site_url")
        final_url = site_url + url
        url_in_text = str(final_url)
        if "en/" in url_in_text:
            url_in_text_fr = url_in_text.replace("en/dataset","fr/dataset")
            #print("case en/, url_in_text: %s, url_in_text_fr:%s "%(url_in_text,url_in_text_fr))
        elif "fr/" in url_in_text:
            url_in_text_fr = url_in_text
            url_in_text = url_in_text_fr.replace("fr/dataset","en/dataset")
            #print("case fr/, url_in_text: %s, url_in_text_fr:%s "%(url_in_text,url_in_text_fr))
        else:
            url_in_text_fr = url_in_text.replace("dataset","fr/dataset")
            #print("case other, url_in_text: %s, url_in_text_fr:%s "%(url_in_text,url_in_text_fr))
        if state == "active":
            body ="""
                (Un message en français suit le texte anglais.)
                This dataset is now ready to be reviewed.(%s)

                Ce jeu de données est maintenant prêt pour révision.(%s)
                """%(url_in_text,url_in_text_fr)

            self.send_email(body,subject)

    def authz_add_role(self, object_role):
        pass

    def authz_remove_role(self, object_role):
        pass

    def delete(self, entity):
        pass

    def before_search(self, search_params):
        #This is for optimization purpuse
        #There's a bug fix dealing with CKAN saving org in 2 places hence caused bug after org name changes
        #However the bug fix caused extra before search activity which can be reduced by return the search
        #paramters without further steps to avoid unecessary search activities

        extras = search_params.get('extras')
        special_chars = ('*', '"', '?')
        lang = get_lang()
        c.result_set = 25
        #mapping
        def get_mapping(q):
            
            def get_keyword_trans(value):
                ''' 
                    This reads keywords from the canada_keywords choices field. It will translate french or english
                    to the corresponding value
                '''
                this_q = value.lower()
                print(this_q)
                # Shortcut until french keywords work
                return value
                key_map_value = this_q
                with open(r'/app/ckan/venv/src/lli-ckan/ckanext/aafc/schemas/tbs_presets.yaml') as f:
                    keywords = yaml.load(f, Loader=yaml.FullLoader)
                    keywords = keywords['presets']
                    choices = list(filter(lambda preset: preset['preset_name'] == 'canada_keywords', keywords))[0]['values']['choices']
                    for item in choices:
                        en_value = item['label']['en'].lower()
                        fr_value = item['label']['fr'].lower()

                        key_value = item['value']

                        if this_q == en_value or this_q == fr_value:
                            key_map_value = key_value
                            print(key_map_value)
                            break

                    return key_map_value  

            # return search_params
            custom_filter_value = reverse_ctrlvoc_values
            search_params['defType'] = 'edismax'
            # If we don't specifiy a specific field eg. "title:<thing>""
            if ':' not in q:
                # value = search_params['q']
                # if value:
                #     filter_value = custom_filter_value.get(value, value)
                #     if filter_value != value:
                #         search_params['q'] = "%s and %s" % (value, filter_value)
                #         return search_params
            
                #     filter_value = get_keyword_trans(value)
                #     if filter_value != value:
                #         search_params['q'] = "%s and %s" % (value, filter_value)
                #         print(search_params['q'])
                #         return search_params
                    
                #     search_params['q'] = "%s" % filter_value
                #     print(search_params['q'])
                return search_params

            if 'metadata_modified' in q or 'metadata_created' in q:
                return search_params
            
            field, value = q.split(':')
            field = field.lower()
            value = value.lower()

            custom_filter_en = {
                'theme': 'extras_theme',
                'organization': 'org_title_at_publication',
                'living lab': 'extras_location',
                'format': 'res_format',
                'keywords': 'canada_keywords'
            }
            custom_filter_fr = {
                'theme':'extras_theme',
                'Organisation': 'org_title_at_publication',
                'Laboratoire vivant': 'extras_location',
                'format': 'res_format'
            }
       
            q_mapper_en = {
                'title': 'title_string',
                'description': 'notes',
            }
            q_mapper_fr = {
                'titre': 'title_fr',
                'title': 'title_fr',
                'description': 'notes',
            }
            # Use default expanded search query
            search_params['defType'] = 'edismax'
            if lang == 'en':
                # if the field is a filter move it from 'q' to 'fq'
                if field in custom_filter_en:
                    search_params['fq'] = " +%s:%s" % (
                        custom_filter_en.get(field), custom_filter_value.get(value, value)
                    )
                    search_params['q'] = ''
                else:
                    search_params['q'] = " +%s:%s" % (
                         q_mapper_en.get(field, field), custom_filter_value.get(value, value)
                    )
            # lang is french
            else:
                # if the field is a filter move it from 'q' to 'fq'
                if field in custom_filter_fr:
                    search_params['q'] = ''               
                    search_params['fq'] = " +%s:%s" % (
                        custom_filter_en.get(field), custom_filter_value.get(value, value)
                    )
                else:
                    search_params['q'] = " +%s:%s" % (
                        q_mapper_fr.get(field, field), custom_filter_value.get(value, value)
                )
            return search_params

        # if a query string has been passed
        if("q" in search_params):
            search_params = get_mapping(search_params['q'])

        #  if one or more filters has been selected
        if("fq" in search_params):
            search_params['fq'] = search_params['fq'].replace('canada_keywords'," +extras_keywords")

        return search_params

        # page = h.get_page_number(request.params)
        # limit = globals.app_globals.datasets_per_page
        # if cookie is set, set limit to the value of the cookie
        # if(request.cookies.get('datasets_per_page') is not None):
        #     try:
        #         limit = int(request.cookies.get('datasets_per_page'))
        #         if limit < 1 or limit > 50:
        #             limit = globals.app_globals.datasets_per_page
        #     except ValueError:
        #         pass

    def after_search(self, search_results, search_params):
  
        lang = get_lang()

        pr = sh.scheming_get_preset("canada_keywords")
        choices = sh.scheming_field_choices(pr)
        #for org_title_at_publication
        pr_org = sh.scheming_get_preset("aafc_org_title_at_publication")
        choices_org = sh.scheming_field_choices(pr_org)
        #for theme
        pr_theme = sh.scheming_get_preset("aafc_theme")
        choices_theme = sh.scheming_field_choices(pr_theme)
        #for location
        pr_location = sh.scheming_get_preset("aafc_location")
        choices_location = sh.scheming_field_choices(pr_location)

        facets = search_results.get('search_facets')

        keys  = search_results.get('search_facets').keys()

        if not facets:
            return search_results
        keywordsInResults = set()
        keywordCounts = {}

        #regroup the keywords because multiple choice return an array of keywords
        can_keywords_items = self.merge_group(facets)
        can_keywords = {"items": can_keywords_items, "title":"canada_keywords"}
        facets['canada_keywords'] = can_keywords 

        if("results" in search_results):
            for each_result in search_results['results']:

                #log.info(">>>One of each result" + json.dumps(each_result))
                if 'en' in each_result['keywords'] or 'fr' in each_result['keywords']:
                    for each_keyword in each_result['keywords'][lang]:
                        keywordsInResults.add(each_keyword)
                        if each_keyword in keywordCounts:
                            keywordCounts[each_keyword] = keywordCounts[each_keyword] + 1
                        else:
                            keywordCounts[each_keyword] = 1
                #handle standalone
                if 'en' not in each_result['keywords'] and 'fr' not in each_result['keywords']:
                    for each_keyword in each_result['keywords']:
                        keywordsInResults.add(each_keyword)
                        if each_keyword in keywordCounts:
                            keywordCounts[each_keyword] = keywordCounts[each_keyword] + 1
                        else:
                            keywordCounts[each_keyword] = 1
            # Put correct label on the items
            for key, facet in facets.items():
                if key not in ['keywords', 'org_title_at_publication','canada_keywords','extras_theme','extras_location']:
                    continue

                if key in ['org_title_at_publication','canada_keywords','extras_theme','extras_location']:
                    for item in facet['items']:
                        field_value = item['name']               
                        if key == 'canada_keywords':
                            label = sh.scheming_choices_label(choices,field_value)
                        elif key == 'org_title_at_publication':
                            label = sh.scheming_choices_label(choices_org,field_value)
                        elif key == 'extras_location':
                            label = sh.scheming_choices_label(choices_location,field_value)
                        else:
                            label = sh.scheming_choices_label(choices_theme,field_value)

                        item['display_name'] = label

        #remove Tags on left
        if lang == 'en':
            kw_title = u'Keywords'
            org_name = u'Organization'
        else:
            kw_title = u'Mots clés' #'Mots clés'
            org_name = u'Organisation'
        try:
            c.facet_titles['canada_keywords'] = kw_title #_('Keywords')
            c.facet_titles['org_title_at_publication'] = org_name#_('Organization Name')

            key_order = (u'res_format', 'canada_keywords','extras_location', 'extras_theme',  'org_title_at_publication', 'private')
            new_queue = OrderedDict()
            for k in key_order:
                if k in c.facet_titles:
                    new_queue[k] = c.facet_titles[k]
            c.facet_titles = new_queue
        except (AttributeError, RuntimeError):
            pass
        facets  = search_results.get('search_facets')
        keys  = search_results.get('search_facets').keys()

        return search_results

    def merge_group(self, facets):
        '''
        because of multiple choice, the returned facets is not in a format like {name:keyword, count:1}
        but instead in { name:"[keyword1, kewyord2]",count:1 
        It needs to be regroup to get count for single keyword for filtering pane rendering
        :param facets: the returned facets in original format
        :return: keywords in correct format 
        '''

        if 'keywords' not in facets:
            return []
        keywords = facets['keywords']
        items = facets['keywords']['items']
        grouped_item_count = {}
        for item in items:
            item_count = item['count']
            names_in_string = item['name']
            names = json.loads(names_in_string)
            for name in names:
                if name in grouped_item_count:
                    grouped_item_count[name] += item_count
                else:
                    grouped_item_count[name] = item_count
        grouped_data = []
        for key, value in grouped_item_count.items():
            item = {"count": value, "name": key, "display_name": key}
            grouped_data.append(item)
        return grouped_data

    def after_show(self, context, data_dict):
        return data_dict

    def update_facet_titles(self, facet_titles):
        log.info(">>>facet_titles: " + json.dumps(facets))
        return facet_titles

    def before_index(self, data_dict):
        def trans_keys(list_of_keywords):
            french_keywords = []
            en_keywords = []
            for keyword in list_of_keywords:          
                with open(r'/app/ckan/venv/src/lli-ckan/ckanext/aafc/schemas/keyword_presets.yaml') as f:
                    keywords = yaml.load(f, Loader=yaml.FullLoader)
                    keywords = keywords['presets']
                    choices = list(filter(lambda preset: preset['preset_name'] == 'canada_keywords', keywords))[0]['values']['choices']
                    for item in choices:
                        en_value = item['label']['en'].lower()
                        fr_value = item['label']['fr'].lower()
                        key_value = item['value']

                        if keyword == key_value:
                            french_keywords.append(fr_value)
                            en_keywords.append(en_value)
                            break
            
                       

            return ' '.join(french_keywords), ' '.join(en_keywords)

        output_file = "/tmp/b4index_" + strftime("%Y-%m-%d_%H_%M_%S", gmtime()) + ".json"

        titles = json.loads(data_dict.get('title_translated', '{}'))
        data_dict['title_fr'] = titles.get('fr', '')
        data_dict['title_fr'] = data_dict['title_fr'].encode('utf-8', 'ignore').decode('utf-8', 'ignore').lower()
        data_dict['title_string'] = titles.get('en', '').lower()

        titles = json.loads(data_dict.get('title_translated', '{}'))
        title_fr = titles.get('fr', '').encode('utf-8', 'ignore').decode('utf-8', 'ignore')
        titles['fr'] = title_fr
        data_dict['extras_title_translated'] = titles
        # print(titles)
        notes = json.loads(data_dict.get('notes_translated', '{}'))
        notes_fr = notes.get('fr', '').encode('utf-8', 'ignore').decode('utf-8', 'ignore')
        notes['fr'] = notes_fr
        # print(notes)
        data_dict['extras_notes_translated'] = notes

        keywords_list_fr, keywords_list_en = trans_keys(json.loads(data_dict.get('extras_keywords', '{}')))
        data_dict['extras_keywords_fr'] = keywords_list_fr
        data_dict['extras_keywords_en'] = keywords_list_en

        
        
        # print(data_dict)
        return data_dict

    def before_view(self, pkg_dict):
        org_id = pkg_dict['organization']['id']
        data_param = {'id': org_id, 'include_users': False}
        self.extra_org_search = True
        organization =  tk.get_action('organization_show')(data_dict = data_param)
        self.extra_org_search = False
        pkg_dict['organization']['title'] = organization['display_name']
        pkg_dict['organization']['name'] = organization['name']
        return pkg_dict

    def after_delete(self, context, data_dict):
        return data_dict

    def after_show(self, context, data_dict):
        return data_dict

    def update_facet_titles(self, facet_titles):

        return facet_titles

    def after_update(self, context, pkg_dict):
        keys = context.keys()
        return pkg_dict


    def after_create(self, context, pkg_dict):
        keys = context.keys()
        return pkg_dict

    def before_map(self, route_map):
        with routes.mapper.SubMapper(route_map,controller='ckanext.aafc.plugin:AAFCController') as m:
            m.connect('addgeopage', '/addgeopage',
                    action='addgeopage')
            m.connect('disclaimer', '/disclaimer',
                    action='disclaimer')
            m.connect('cdtsMenu', '/cdtsMenu',
                    action='cdtsMenu')
        return route_map

    def get_blueprint(self):
        return blueprint.get_blueprints()


    def after_map(self, route_map):
        return route_map

    #IResourceController
    
    def before_create(self, context, resource):
        pass

    def after_create(self, context, resource):
        session["created"]= True
        #log.info('>>>>after create:resource')

        user = context['user']
        model = context['model']
        pkg = model.Package.get(resource['id'])
        context_session = context['session']

        user_obj = model.User.by_name(user)
        if user_obj:
            user_id = user_obj.id
        else:
            user_id = 'not logged in'

        # If the package is private, add new activity stream item
        if pkg is not None and pkg.private:
            activity = pkg.activity_stream_item('new', user_id)
            context_session.add(activity)

            if not context.get('defer_commit'):
                model.repo.commit()
        pass

    def before_update(self, context, current, resource):
        pass

    def after_update(self, context, resource):
        is_created = session.get("created",False)            
        session["created"] = False
        rstate = resource.get('state','default')

        # If the package is new, we do not want to create a new activity as this has been done by the 
        # after_create method
        if not is_created:
            keys = context.keys()
            user = context['user']
            model = context['model']
            package = context['package']
            context_session = context['session']

            user_obj = model.User.by_name(user)
            if user_obj:
                user_id = user_obj.id
            else:
                user_id = 'not logged in'

            # In the core, package activity is added only if the package is public. Here, we want to add the activity
            # only if the package is private (to avoid duplicate 'changed' activities)
            if package.private:
                activity = package.activity_stream_item('changed', user_id)
                context_session.add(activity)

                if not context.get('defer_commit'):
                    model.repo.commit()


    def before_delete(self, context, resource, resources):
        pass

    def after_delete(self, context, resources):
        pass

    def before_show(self, resource):
        pass

    def send_email(self, body, subject=None):
        disable_email = config.get("aafc_disable_email", "false")
        if disable_email == "true":
            print(">>>>Mock send email")
            print(f"subject:{subject}")
            print(f"body:{body}")
            return
        sender = config.get("aafc_mail_from","admin@canada.ca")
        if subject is None:
            subject = config.get("aafc_subject","A dataset created/updated")

        smtp_server = config.get("smtp.server").split(":")[0]
        smtp_user = config.get("smtp.user")
        smtp_passowrd = config.get("smtp.password")

        recipient = config.get("aafc_mail_to","admin@canada.ca")
        receivers = [recipient]

        message = MIMEText(body)
        message["Subject"] = subject
        message["From"] = sender
        message["To"] = recipient

        try:
            ckan.lib.mailer.mail_recipient(sender, recipient ,subject ,body)
            smtpObj = smtplib.SMTP(smtp_server,587)
            smtpObj.starttls()
            smtpObj.login(smtp_user, smtp_passowrd)
            # smtpObj.sendmail(sender, receivers, message.as_string())    
            log.info("###>>> Email sent()")
        except (ckan.lib.mailer.MailerException, socket.error) as e:
            log.info(e)
