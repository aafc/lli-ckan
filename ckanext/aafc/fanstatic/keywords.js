// constant declarations for html elements

// multiselect for the selected keywords
const selectedOptions = document.getElementById('package_form_selected_keywords');
let url;
$("#package_form_keyword_select").empty();
var dropDown = document.getElementById("package_form_keyword_select");
if (current_path.includes("/fr/")) {
    url = "/fr/api/3/action/"
} else {
    url = "/api/3/action/"  
}

// Get the DataProvider name on dataset view page without restarting the server to reload yaml
var provider = $('#data-provider').text().trim();
$.ajax({
    type: "GET",
    url: url + "get_yaml",
    success: function(data){
    var opts = data['result'];
        var providerName;
        $.each(opts, function(i, d) {
            if (d.value == provider){
         
                providerName = d.name
                
            }
        });
        $('#data-provider').text(providerName)
    }
    
});

$.ajax({
        type: "GET",
        url: url + "get_keyword_yaml",
        success: function(data){
            var opts = data['result'];

            $.each(opts, function(i, d) {
                $('#package_form_keyword_select').append('<option id="field-keywords-'+d.value+ '" class="keyword_option" value="' + d.value + '">' + d.name + '</option>');
            });
            let options = document.getElementById('package_form_keyword_select');

            // hidden multiselect for data submission
            if (selectedOptions) {
                for(let i = 0; i < selectedOptions.length; i++)
                {
                    for(let j = 0; j < options.length; j++)
                    {
                        if(options[j].value == selectedOptions[i].value)
                        {
                            options[j].classList.add('hidden');
                        }
                    }
                }
            }
        }       
    });

const selectedOptionsHidden = document.getElementById('selected_keywords_hidden');

// Buttons for add, remove and reset actions
const addButton = document.getElementById('btn_keyword_add');
const removeButton = document.getElementById('btn_keyword_remove');
if (addButton != null) {
    // add button handler
    addButton.onclick = function(){
        // find all selected values
        const selectedValues = $('#package_form_keyword_select :selected');
        // $('#package_form_keyword_select ').remove();

        // for each selected value, check if the keyword has already been selected

        for(let i = 0; i < selectedValues.length; i++)
        {
            let found = false;
            $('#package_form_selected_keywords>option').each(function(){
                if(this.value == selectedValues[i].value)
                {
                    found = true;
                }
            });

            // if the keyword has not already been selected, add option to the righthand box
            // and to the hidden multiselect used for form submission
            if(!found)
            {
                // create new html element to place in selected keywords box
                const newOption = document.createElement('option');
                newOption.innerHTML = selectedValues[i].innerHTML;
                newOption.value = selectedValues[i].value;
                selectedOptions.appendChild(newOption);

                // Create new html element to place in hidden multiple select input for submission
                const hiddenOption = document.createElement('option');
                hiddenOption.innerHTML = selectedValues[i].innerHTML;
                hiddenOption.value = selectedValues[i].value;
                hiddenOption.selected = true;
                selectedOptionsHidden.appendChild(hiddenOption);

                // console.log(selectedValues[i].value)
                $('#package_form_keyword_select').find('[value="' + selectedValues[i].value + '"]' ).addClass('hidden');
            }
            // deselect selected values in left hand box
            selectedValues[i].selected = false;

        }
    };

}
try {
    removeButton.onclick = function() {
        // get all selected items from right hand multiselect box
        const selectedValues = $('#package_form_selected_keywords :selected');
        const selectedOptionsHidden = $('#selected_keywords_hidden');
        const selectedValuesHidden = $('#selected_keywords_hidden :selected');

        // remove items
        for(let i = 0; i < selectedValues.length; i++)
        {
            // console.log(selectedValues[i]);
            selectedValues[i].remove();
            selectedOptionsHidden.find('[value="' + selectedValues[i].value + '"]' ).remove();
        
        var thing = $('#package_form_keyword_select').find('[value="' + selectedValues[i].value + '"]' );
        if (thing.length > 0) {
            thing.removeClass('hidden');
    };

        }
    }
} catch (TypeError) {
    // wrong page XXX fixme
}

$(".pagination").find(".disabled").removeClass("disabled");


var currentDataProviderVal = $("#field-org_title_at_publication").find(":selected").val();
var currentDataProviderText = $("#field-org_title_at_publication").find(":selected").text();
// console.log(currentDataProviderVal);
// console.log(currentDataProviderText);
$("#field-org_title_at_publication").empty();

var dropDown = document.getElementById("field-org_title_at_publication");

let currentUrlArray = new URL(window.location).pathname.split('/');

var packageID = currentUrlArray.pop();
var operation = currentUrlArray.pop();
if (operation == 'edit') {
    $.ajax({
        type: "GET",
        url: url + "package_show?id=" + packageID,
        success: function(data){
            var provider = data['result']['org_title_at_publication'];
            $.ajax({
                type: "GET",
                url: url + "get_yaml",
                success: function(data){
                    var opts = data['result'];
        
                    $.each(opts, function(i, d) {
                        if (!d.deleted) {
                            $('#field-org_title_at_publication').append('<option value="' + d.value + '">' + d.name + '</option>');
                        }
                    });
                    if ($('#field-org_title_at_publication option[value="' + currentDataProviderVal + '"]').length > 0) {
                        // The provider exists in the list
                
                    } else {
                        var providerValue;
                        $.each(opts, function(i, d) {
                            if (d.value == provider){
                                providerValue = d.name
                                
                            }
                        });
                        $('#field-org_title_at_publication').append('<option value="' + provider + '">' + providerValue + '</option>');
                }
                    $("#field-org_title_at_publication").val(provider);
                }
                
            });
        }     
    });
} else if (operation == 'new' || packageID == 'new') {
    $.ajax({
        type: "GET",
        url: url + "get_yaml",
        success: function(data){
            var opts = data['result'];
            $('#field-org_title_at_publication').append('<option></option>');
            $.each(opts, function(i, d) {
                if (!d.deleted) {
                    $('#field-org_title_at_publication').append('<option value="' + d.value + '">' + d.name + '</option>');
                }
            });
        }       
    });
}
