console.log("add resource");
checkPrivate();
hideFieldsNotUsedFromRegistry();

function getToday(){
	var now = new Date(); 
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	return  now.getFullYear()+"-"+(month)+"-"+(day) ;
}


if ($("#field-date_published").val() == "") {
	var date = getToday();
	$("#field-date_published").val(date);

}

function checkPrivate(){
	//NPE check
	if($("a[href*='/dataset/resources']")[0] == null)
		return;
	var dataset_edit_url = $("a[href*='/dataset/resources']")[0].href;
	var dataset_id = dataset_edit_url.substring(dataset_edit_url.lastIndexOf('/') + 1);
	var dataset_path = window.location.origin + "/api/3/action/package_show?id=" + dataset_id;
	var is_private = false;
	$.ajax( {
		url : dataset_path,
		type : "GET",
		success : function(data) {
			// Allow public editing if admin in Org.
			if(data.result.private == false) {
				//Am I admin? Get org, and the current user, cross ref capacity
				//org id
				var org_url = $("[href*='/organization/']")[0].href;
				var org_id = org_url.substring(org_url.lastIndexOf('/') + 1);

				//retrieve org
				var org_path = window.location.origin + "/api/3/action/organization_show?id=" + org_id;

				$.ajax( {
					url : org_path,
					type : "GET",
					success : function(data) {
						var canEditPublic = false;

						//get current user
						var user_url = $("[href*='/user']")[0].href;
						var user_id = user_url.substring(user_url.lastIndexOf('/') + 1);

						var org_users = data.result.users;
						org_users.forEach(user => {
							if(user.name == user_id && user.capacity == "admin")
								canEditPublic = true;
						});

						if(!canEditPublic) {
							console.log("PUBLIC via AJAX1!!");

							//disable add/save button
							$("[name='save']")[0].disabled = true;
							//show error, hide form and other fields
							$('#errorMsg').text("ERROR: The dataset must be private before you can add a resource.");
						}
					}
				})


			}

		}
	});


}

function hideFieldsNotUsedFromRegistry(){
	$("#field-size").val("1");
	$("#field-character_set").parent().parent().hide();
	$("#field-size").parent().parent().hide();
}

$("input[alttype='date']").focus( function(e){
    e.target.type='date';
});

$("input[alttype='date']").blur( function(e){
    e.target.type='text';
});	

$("input[alttype='date']").on("input", function(e){
    e.target.type='text';
    e.target.blur();
});	
