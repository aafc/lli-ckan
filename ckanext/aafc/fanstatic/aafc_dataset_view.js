function hideCoreFieldsNotUsed(){
	var stateTH = null;
	if($("th:contains('État')").length == 1){
		var stateTH = $("th:contains('État')");
	} else if($("th:contains('State')").length == 1){
		var stateTH = $("th:contains('State')");
	}
	
	if(stateTH != null){
		stateTH.parent().hide();
	}
	console.log("hideCoreFieldsNotUsed COMPLETE");

}

if (window.location.pathname.includes("dataset") || !window.location.pathname.includes("/edit/")){
   hideCoreFieldsNotUsed();
}


function check_role(org_id,call_back){
    var org_path = window.location.origin + "/api/3/action/organization_show?id=" + org_id;
    var role = "internal"; //default as lowest
     $.ajax( {
        url : org_path,
        type : "GET",
        success : function(data) {
            //get current user
            var user_url = $("[href*='/user']")[0].href;
            var user_id = user_url.substring(user_url.lastIndexOf('/') + 1);
            var org_users = data.result.users;
            org_users.forEach(user => {
               if(user.name == user_id)
		    role = user.capacity;
            });
	    call_back(role);
	
        }
    })

}


var aafc_author = $("#field-aafc_author").text()
var org_id = $("#field-owner_org").text();


//only when private
if($(".dataset-private").length > 0){
    check_role(org_id, function(r){ 
        var user_url = $("[href*='/user']")[0].href;
        var user_id = user_url.substring(user_url.lastIndexOf('/') + 1);
		//if( r != "admin" && aafc_author != user_id)
        if( r != "admin" && r != "editor")			
	    $(".content_action>a").removeAttr('href').addClass('disabled');
    });
}


var elem = document.getElementsByTagName('a');
    for (i = 0; i < elem.length; i++) {
        if (elem[i].getAttribute("href") == "") {
        elem[i].remove();             }
    }

$(".pagination").find("disabled").removeClass( "disabled" );