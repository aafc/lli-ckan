var waitForEl = function(selector, callback) {
    if (jQuery(selector).length) {
        callback();
    } else {
        setTimeout(function() {
            waitForEl(selector, callback);
        }, 100);
    }
};

waitForEl(".geomap-help-btn", function() {
    $(".geomap-help-btn").click(function() {
        $(".geomap-help-dialog").click(function() {
            $(".geomap-help-dialog").remove();
        });
    });
});