// Called by the apply filters button onclick handler

function clear_filters() {
    // document.getElementsByClassName("clearButton").onclick = function () {
        console.log('here');
        document.getElementById('search_field').value = "";
        let url = new URL(window.location);
        newUrl = new URL(window.location.origin + url.pathname);
        window.location = newUrl.href;
    //   }
}

function apply_filters()
{
    // Form parameter list from active list-group-item elements
    let search_categories = [];
    let search_items = [];
    $(".list-group-item.active").find(".filter_category_name").each(function(index, element){
        search_categories.push(element.value.trim());
    });
    $(".list-group-item.active").find(".filter_item_name").each(function(index, element){
        search_items.push(element.value.trim());
    });
    
    // Create URL from parameter list

    // Get current URL to set current q and sort parameters
    let url = new URL(window.location);
    console.log(url.pathname);
    let searchParameters = new URLSearchParams(url.search);

    // Create new URL

    let newUrl;
    newUrl = new URL(window.location.origin + url.pathname);
    // if (url.pathname.includes('/fr/')) {
    //     newUrl = new URL(window.location.origin + "/fr/dataset/");
    // }
    // else {
    //     newUrl = new URL(window.location.origin + "/dataset/");
    // }
    // If previous URL had q parameter, append to new URL
    if(searchParameters.has('q'))
    {
        newUrl.searchParams.append('q', searchParameters.get('q'));
    }

    // Add search parameters from active items to new URL
    for(var i = 0; i < search_categories.length; i++)
    {
        newUrl.searchParams.append(search_categories[i], search_items[i]);
    }

    // If previous URL had specified order-by method, add corresponding sort parameter
    if(searchParameters.has('sort'))
    {
        newUrl.searchParams.append('sort', searchParameters.get('sort'));
    }

    var result_limit = $('#field-row-number').find(":selected").text();
    newUrl.searchParams.append('ext_result_number', result_limit);
    // Redirect to new URL
    window.location = newUrl.href;
}

// Toggles the active class on the filter DOM elements. Triggered by the filter checkboxes onchange
function toggle_filter_box_active(element)
{
    // Find the category of the filter that got checked
    const filterCategory = element.parentNode.nextSibling.nextSibling.value;

    // Find first parent with class 'list-group-item'
    let active_box = element;
    while(!active_box.classList.contains('list-group-item'))
    {
        active_box = active_box.parentNode;
    }

    // If element has active class, remove active class. Else, add active class
    if(active_box.classList.contains('active'))
    {
        active_box.classList.remove('active');
    }
    else
    {
        // If filter category is living labs location, remove all other active elements in the living labs
        // category (a dataset can only have one living lab location)
        if(filterCategory == 'extras_location')
        {
            // Find the first active item in the living lab location category
            let listItem = active_box.parentNode.firstElementChild;

            // loop through all the filters to assert inactive state
            while(listItem != null){
                listItem.classList.remove('active');
                $(listItem).find(".dataset_filter_checkbox")[0].checked = false;
                // go to next item
                listItem = listItem.nextElementSibling;
            }
        }

        // Toggle active class
        active_box.className +=' active';
        $(active_box).find(".dataset_filter_checkbox")[0].checked = true;
    }
}
